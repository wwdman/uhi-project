---
title: "Discovery Project - Exploration and Modeling"
author: "Daniel Stratti 18577857"
date: "27/10/2019"
output: word_document
---
```{r setup, include=FALSE}
# Install packages
#install.packages('dplyr')
#install.packages('tidyr')
#install.packages("ggplot2")
#install.packages("Amelia")
#install.packages("caTools")
# install.packages("ggfortify")

# Import Libraries
library(dplyr)
library(tidyr)
library(ggplot2)
library(Amelia)
library(caTools)
library(gridExtra)
library(ggfortify)
```

## Data Amalgamate
The code within the file 18577857 DS DP - Data Amalgamation must be run proir to executing this code to create the required dataset.

The data has been amaligimated from three different sources, the Bureau of Meteorology (BOM), the CSIRO Soil and Landscape Grid of Australia (SLGA) and the AUSTRALIAN BUREAU OF STATISTICS (ABS) Census from 1991 - 2016. The data will now be explored and standardised in preperation for statistical analysis. 

# Data Exploration
## Initial Data Visulaisation
```{r}
uhi_data = read.csv("uhi_dataset.csv")
dim(uhi_data)
str(uhi_data)

## Visualise Missing data
missmap(uhi_data, main = 'Missing Map', col = c("yellow", "black"),
        legend = FALSE)
```

## Standardising the data
Currently the Census housing data is in raw counts, this needs to be standardised in relation to the surface area of the city.

Additionally the temperatures are relative to the cities height above sea-level, to account for this, all temperatures will be divided by the cities air presure

```{r}
uhi_data_std = uhi_data

uhi_data_std_date = separate(uhi_data_std, Date, sep='-', into=c("Year", "Month", "Day"), remove = F)
uhi_data_std_date = unite(uhi_data_std_date, MonthDay, c('Month', 'Day'), sep = '_', remove = F)
uhi_data_std_date$Month = as.numeric(uhi_data_std_date$Month)
uhi_data_std_date$Day = as.numeric(uhi_data_std_date$Day)


# Fill in missing values for Temp Min, Max, Precipitation & Solar Radiation
a = uhi_data_std_date %>% 
  group_by(MonthDay, city) %>%
  summarise(
    mean_temp_max = mean(Temp.Max, na.rm = TRUE), 
    mean_temp_min = mean(Temp.Min, na.rm = TRUE), 
    mean_solar = mean(Solar.Radiation..MJ.m.m., na.rm = TRUE), 
    mean_rain = mean(Precipitation, na.rm = TRUE),
    min_other = min(Other, na.rm = TRUE),
    min_notStated = min(Not.Stated, na.rm = TRUE),
    min_semiDetached = min(Semi.detached, na.rm = TRUE),
    min_sepHouse = min(Separate.house, na.rm = TRUE),
    min_flat = min(Flat, na.rm = TRUE)
  ) 

write.csv(a, "avg_bom_values.csv", row.names = F)

avgTemp = read.csv("avg_bom_values.csv")

for (i in 1:nrow(uhi_data_std_date)) {
  
  # Fill in NA temp max values
  if (is.na(uhi_data_std_date[i, "Temp.Max"])) {
    
    uhi_data_std_date[i, "Temp.Max"] = avgTemp[avgTemp$MonthDay == uhi_data_std_date[i, "MonthDay"] & avgTemp$city == uhi_data_std_date[i, "city"], "mean_temp_max"]
  }
  
  # Fill in NA temp min values
  if (is.na(uhi_data_std_date[i, "Temp.Min"])) {
    
    uhi_data_std_date[i, "Temp.Min"] = avgTemp[avgTemp$MonthDay == uhi_data_std_date[i, "MonthDay"] & avgTemp$city == uhi_data_std_date[i, "city"], "mean_temp_min"]
  }
  
  # Fill in NA solar values
  if (is.na(uhi_data_std_date[i, "Solar.Radiation..MJ.m.m."])) {
    
    uhi_data_std_date[i, "Solar.Radiation..MJ.m.m."] = avgTemp[avgTemp$MonthDay == uhi_data_std_date[i, "MonthDay"] & avgTemp$city == uhi_data_std_date[i, "city"], "mean_solar"]
  }
  
  # Fill in NA temp min values
  if (is.na(uhi_data_std_date[i, "Precipitation"])) {
    
    uhi_data_std_date[i, "Precipitation"] = avgTemp[avgTemp$MonthDay == uhi_data_std_date[i, "MonthDay"] & avgTemp$city == uhi_data_std_date[i, "city"], "mean_rain"]
  }
  
  
  # Fill in NA Other (Housing) values with 0
  if (is.na(uhi_data_std_date[i, "Other"])) {
    
    uhi_data_std_date[i, "Other"] = 0
  }
  
  
  # fix surface area
  if (is.na(uhi_data_std_date[i, "Surface.Area"]))
  {
    uhi_data_std_date[i, "Surface.Area"] = 3.1
  }
  
  
  if (uhi_data_std_date[i, "Surface.Area"] <= 0)
  {
    uhi_data_std_date[i, "Surface.Area"] = 3.1
  }
  
  
  # fix Katoomba surface areas
  if (uhi_data_std_date[i, "city"] == "katoomba" && uhi_data_std_date[i, "Surface.Area"] < 19.4) {
    uhi_data_std_date[i, "Surface.Area"] = 19.4
  }
  
  # fix parramatta surface areas
  if (uhi_data_std_date[i, "city"] == "parramatta" && uhi_data_std_date[i, "Surface.Area"] < 3.1) {
    uhi_data_std_date[i, "Surface.Area"] = 3.1
  }
  
  # fix penrith surface areas
  if (uhi_data_std_date[i, "city"] == "penrith" && uhi_data_std_date[i, "Surface.Area"] < 404.6) {
    uhi_data_std_date[i, "Surface.Area"] = 404.6
  }
  
  
  
  
  ## Fix flats,Separate.house  Semi.detached Flat Other Not.Stated Population cars 
  if (!is.na(uhi_data_std_date[i, "cars"]) && uhi_data_std_date[i, "cars"] <= 0)
  {
    uhi_data_std_date[i, "cars"] = 0
  }

  if (!is.na(uhi_data_std_date[i, "Population"]) && uhi_data_std_date[i, "Population"] <= 0)
  {
    uhi_data_std_date[i, "Population"] = 0
  }

  if (!is.na(uhi_data_std_date[i, "Other"]) && uhi_data_std_date[i, "Other"] <= 0)
  {
    uhi_data_std_date[i, "Other"] = 0
  }

  if (!is.na(uhi_data_std_date[i, "Flat"]) && uhi_data_std_date[i, "Flat"] <= 0)
  {
    uhi_data_std_date[i, "Flat"] = 0
  }

  if (!is.na(uhi_data_std_date[i, "Semi.detached"]) && uhi_data_std_date[i, "Semi.detached"] <= 0)
  {
    uhi_data_std_date[i, "Semi.detached"] = 0
  }

  if (!is.na(uhi_data_std_date[i, "Separate.house"]) && uhi_data_std_date[i, "Separate.house"] <= 0)
  {
    uhi_data_std_date[i, "Separate.house"] = 0
  }

  if (!is.na(uhi_data_std_date[i, "Not.Stated"]) && (uhi_data_std_date[i, "Not.Stated"] <= 0))
  {
    uhi_data_std_date[i, "Not.Stated"] = 0
  }
}

# Census Stadardisation
uhi_data_std_date$Separate.house.std = NA
uhi_data_std_date$Semi.detached.std  = NA
uhi_data_std_date$Flat.std  = NA
uhi_data_std_date$Other.std  = NA
uhi_data_std_date$Not.Stated.std  = NA
uhi_data_std_date$Population.std  = NA
uhi_data_std_date$cars.std  = NA



uhi_data_std_date$Separate.house.std = (uhi_data_std_date$Separate.house/uhi_data_std_date$Surface.Area)
uhi_data_std_date$Semi.detached.std  = (uhi_data_std_date$Semi.detached/uhi_data_std_date$Surface.Area)
uhi_data_std_date$Flat.std  = (uhi_data_std_date$Flat/uhi_data_std_date$Surface.Area)
uhi_data_std_date$Other.std  = (uhi_data_std_date$Other/uhi_data_std_date$Surface.Area)
uhi_data_std_date$Not.Stated.std  = (uhi_data_std_date$Not.Stated/uhi_data_std_date$Surface.Area)
uhi_data_std_date$Population.std  = (uhi_data_std_date$Population/uhi_data_std_date$Surface.Area)
uhi_data_std_date$cars.std  = (uhi_data_std_date$cars/uhi_data_std_date$Surface.Area)


#uhi_data_std_date[is.na(uhi_data_std_date$Not.Stated), ]

 na.omit((uhi_data_std_date[uhi_data_std_date$city == "katoomba", "Separate.house"]/uhi_data_std_date[uhi_data_std_date$city == "katoomba", "Surface.Area"]))

uhi_data_std_date$builtEnv = NA
uhi_data_std_date$builtEnv = uhi_data_std_date$Separate.house.std + uhi_data_std_date$Semi.detached.std + uhi_data_std_date$Flat.std + uhi_data_std_date$Not.Stated.std + uhi_data_std_date$Other.std

# Weather Stadnardisation
uhi_data_std_date$sea.level = NA

uhi_data_std_date[uhi_data_std_date$city == "penrith", "sea.level"] = 63 ## 30.17
uhi_data_std_date[uhi_data_std_date$city == "parramatta", "sea.level"] = 43
uhi_data_std_date[uhi_data_std_date$city == "katoomba", "sea.level"] = 1017

# Atmospheric preasure
## P= P0 * exp(− (Mg/RT)h)
p0 = 101325
M = 0.02896 # Molar mass of Earth's air
g = 9.807 # accelleration due to gravity
Temp = 288.15 # Standard temp in kelvin
R = 8.3143 # Universal gas constant

uhi_data_std_date$atmos = NA
uhi_data_std_date$atmos = p0 * exp(-(((M*g)/(R*Temp))*uhi_data_std_date$sea.level)) 

uhi_data_std_date$Temp.Max.std = ( ( log(uhi_data_std_date$Temp.Max  )) / (uhi_data_std_date$atmos))
uhi_data_std_date$Temp.Min.std = ( ( log(uhi_data_std_date$Temp.Min  )) / (uhi_data_std_date$atmos))

missmap(uhi_data_std_date, main = 'Missing Map', col = c("yellow", "black"),
        legend = FALSE)


write.csv(uhi_data_std_date, "uhi_data_std_date.csv", row.names = F)

```



### Plot Census Data Vs Date
```{r}
uhi_data_std_date$Date = as.Date(uhi_data_std_date$Date)

# Plot the Seperate housing
ggplot(uhi_data_std_date, aes(as.Date(Date), Separate.house.std)) +
  geom_line(aes(colour = factor(city)), size = 3) +
  labs(title = "Seperate housing Vs Date (Penrith, Parramatta & Katoomba)")

# Plot the Semi-detached housing
ggplot(uhi_data_std_date, aes(as.Date(Date), Semi.detached.std)) +
  geom_line(aes(colour = factor(city)), size = 3) +
  labs(title = "Semi-detached housing Vs Date (Penrith, Parramatta & Katoomba)")


# Plot the Flat housing
ggplot(uhi_data_std_date, aes(as.Date(Date), Flat.std)) +
  geom_line(aes(colour = factor(city)), size = 3) +
  labs(title = "Flats Vs Date (Penrith, Parramatta & Katoomba)")


# Plot the Other housing
ggplot(uhi_data_std_date, aes(as.Date(Date), Other.std)) +
  geom_line(aes(colour = factor(city)), size = 3) +
  labs(title = "Other housing Vs Date (Penrith, Parramatta & Katoomba)")


# Plot the Not Stated housing
ggplot(uhi_data_std_date, aes(as.Date(Date), Not.Stated.std)) +
  geom_line(aes(colour = factor(city)), size = 3) + 
  labs(title = "Not Stated housing Vs Date (Penrith, Parramatta & Katoomba)")


# Plot the Population
ggplot(uhi_data_std_date, aes(as.Date(Date), Population.std)) +
  geom_line(aes(colour = factor(city)), size = 3) + 
  labs(title = "Population Vs Date (Penrith, Parramatta & Katoomba)")

# Plot the Population
ggplot(uhi_data_std_date, aes(as.Date(Date), cars.std)) +
  geom_line(aes(colour = factor(city)), size = 3) + 
  labs(title = "Cars Vs Date (Penrith, Parramatta & Katoomba)")

# Plot the Built Environment
ggplot(uhi_data_std_date, aes(as.Date(Date), builtEnv)) +
  geom_line(aes(colour = factor(city)), size = 3) + 
  labs(title = "Built Environment Vs Date (Penrith, Parramatta & Katoomba)")

# Plot the Surface Area 

ggplot(uhi_data_std_date, aes(as.Date(Date), Surface.Area)) +
  geom_line(aes(colour = factor(city)), size = 3) + 
  labs(title = "Surface Area Vs Date (Penrith, Parramatta & Katoomba)")
```



### Plot Census Data Vs Temp
```{r}
# Plot the Seperate housing
ggplot(uhi_data_std_date, aes(Separate.house.std, Temp.Max.std)) +
  geom_point(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Max Vs Seperate housing (Penrith, Parramatta & Katoomba)")


# Plot the Semi-detached housing
ggplot(uhi_data_std_date, aes(Semi.detached.std, Temp.Max.std)) +
  geom_point(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Max Vs Semi-detached housing (Penrith, Parramatta & Katoomba)")


# Plot the Flat housing
ggplot(uhi_data_std_date, aes(Flat.std, Temp.Max.std)) +
  geom_point(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Max Vs Flat housing (Penrith, Parramatta & Katoomba)")



# Plot the Other housing
ggplot(uhi_data_std_date, aes(Other.std, Temp.Max.std)) +
  geom_point(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Max Vs Other housing (Penrith, Parramatta & Katoomba)")


# Plot the Not Stated housing
ggplot(uhi_data_std_date, aes(Not.Stated.std, Temp.Max.std)) +
  geom_point(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Max Vs Not Stated housing (Penrith, Parramatta & Katoomba)")


# Plot the Population
ggplot(uhi_data_std_date, aes(Population.std, Temp.Max.std)) +
  geom_line(aes(colour = factor(city)), size = 3) + 
  labs(title = "Temp.Max Vs Population (Penrith, Parramatta & Katoomba)")

# Plot the Not Stated housing
ggplot(uhi_data_std_date, aes(cars.std, Temp.Max.std)) +
  geom_point(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Max Vs Cars (Penrith, Parramatta & Katoomba)")

# Plot the Built Environment
ggplot(uhi_data_std_date, aes(builtEnv, Temp.Max.std)) +
  geom_line(aes(colour = factor(city)), size = 3) + 
  labs(title = "Temp.Max Vs Built Environment (Penrith, Parramatta & Katoomba)")


# Plot the Surface Area 
ggplot(uhi_data_std_date, aes(Surface.Area, Temp.Max.std)) +
  geom_point(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Max Vs Surface Area (Penrith, Parramatta & Katoomba)")
```


```{r}
# Plot the Seperate housing
ggplot(uhi_data_std_date, aes(Separate.house, Temp.Max)) +
  geom_point(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Max Vs Seperate housing (Penrith, Parramatta & Katoomba)")


# Plot the Semi-detached housing
ggplot(uhi_data_std_date, aes(Semi.detached, Temp.Max)) +
  geom_point(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Max Vs Semi-detached housing (Penrith, Parramatta & Katoomba)")


# Plot the Flat housing
ggplot(uhi_data_std_date, aes(Flat, Temp.Max)) +
  geom_point(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Max Vs Flat housing (Penrith, Parramatta & Katoomba)")



# Plot the Other housing
ggplot(uhi_data_std_date, aes(Other, Temp.Max)) +
  geom_point(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Max Vs Other housing (Penrith, Parramatta & Katoomba)")


# Plot the Not Stated housing
ggplot(uhi_data_std_date, aes(Not.Stated, Temp.Max)) +
  geom_point(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Max Vs Not Stated housing (Penrith, Parramatta & Katoomba)")

# Plot the Not Stated housing
ggplot(uhi_data_std_date, aes(Population, Temp.Max)) +
  geom_point(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Max Vs Population (Penrith, Parramatta & Katoomba)")


# Plot the Not Stated housing
ggplot(uhi_data_std_date, aes(cars, Temp.Max)) +
  geom_point(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Max Vs Cars (Penrith, Parramatta & Katoomba)")


# Plot the Surface Area 
ggplot(uhi_data_std_date, aes(Surface.Area, Temp.Max)) +
  geom_point(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Max Vs Surface Area (Penrith, Parramatta & Katoomba)")
```


## Temperature Vs Date
```{r}
# Plot the temp Vs the Date
# -------------------------------------------------------------------------------
ggplot(uhi_data_std_date, aes(as.Date(Date), Temp.Max)) +
  geom_point(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Max Vs Date (Penrith, Parramatta & Katoomba)")

ggplot(uhi_data_std_date, aes(as.Date(Date), Temp.Max.std)) +
  geom_point(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Max.std Vs Date (Penrith, Parramatta & Katoomba)")

ggplot(uhi_data_std_date, aes(as.Date(Date), Temp.Min)) +
  geom_point(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Min Vs Date (Penrith, Parramatta & Katoomba)")

ggplot(uhi_data_std_date, aes(as.Date(Date), Temp.Min.std)) +
  geom_point(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Min.std Vs Date (Penrith, Parramatta & Katoomba)")
# -------------------------------------------------------------------------------


# Plot the temp Vs the city
# -------------------------------------------------------------------------------
ggplot(uhi_data_std_date, aes(city, Temp.Max)) +
  geom_boxplot(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Max Vs city (Penrith, Parramatta & Katoomba)")

ggplot(uhi_data_std_date, aes(city, Temp.Max.std)) +
  geom_boxplot(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Max.std Vs city(Penrith, Parramatta & Katoomba)")

ggplot(uhi_data_std_date, aes(city, Temp.Min)) +
  geom_boxplot(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Min Vs city (Penrith, Parramatta & Katoomba)")

ggplot(uhi_data_std_date, aes(city, Temp.Min.std)) +
  geom_boxplot(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Min.std Vs city (Penrith, Parramatta & Katoomba)")
# -------------------------------------------------------------------------------




# Plot the temp Vs Month
# -------------------------------------------------------------------------------
## Group the teperatures by year to see underlying trends
p1 = ggplot(uhi_data_std_date, aes(as.factor(Month), Temp.Max.std)) +
  geom_boxplot(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Max.std Vs Date (Penrith, Parramatta & Katoomba)")

p2 = ggplot(uhi_data_std_date, aes(as.factor(Month), Temp.Min.std)) +
  geom_boxplot(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Min.std Vs Date (Penrith, Parramatta & Katoomba)")

grid.arrange(p1, p2, nrow=2)

p3 = ggplot(uhi_data_std_date, aes(as.factor(Month), Temp.Max)) +
  geom_boxplot(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Max Vs Date (Penrith, Parramatta & Katoomba)")

p4 = ggplot(uhi_data_std_date, aes(as.factor(Month), Temp.Min)) +
  geom_boxplot(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Min Vs Date (Penrith, Parramatta & Katoomba)")

grid.arrange(p3, p4, nrow=2)
# -------------------------------------------------------------------------------



# Plot the Temp Hist
# -------------------------------------------------------------------------------
p5 = ggplot(uhi_data_std_date,aes(x=Temp.Max)) + 
    geom_histogram(data=subset(uhi_data_std_date,city == 'penrith'),fill = "blue", alpha = 0.4, bins=50) +
    geom_histogram(data=subset(uhi_data_std_date,city == 'parramatta'),fill = "green", alpha = 0.4, bins=50) +
    geom_histogram(data=subset(uhi_data_std_date,city == 'katoomba'),fill = "red", alpha = 0.4, bins=50)+
  labs(title = "Temp.Max (Penrith, Parramatta & Katoomba)")


p6 = ggplot(uhi_data_std_date,aes(x=Temp.Max.std)) + 
    geom_histogram(data=subset(uhi_data_std_date,city == 'penrith'),fill = "blue", alpha = 0.4, bins=50) +
    geom_histogram(data=subset(uhi_data_std_date,city == 'parramatta'),fill = "green", alpha = 0.4, bins=50) +
    geom_histogram(data=subset(uhi_data_std_date,city == 'katoomba'),fill = "red", alpha = 0.4, bins=50) +
  labs(title = "Temp.Max.std (Penrith, Parramatta & Katoomba)")+ 
    scale_fill_manual(name="City", values=c("blue","green", "red"),labels=c("penrith","parramatta", "katoomba"))
  
  
  

grid.arrange(p5, p6, nrow=2)

 
ggplot(uhi_data_std_date,aes(x=Temp.Min)) + 
    geom_histogram(data=subset(uhi_data_std_date,city == 'penrith'),fill = "blue", alpha = 0.4, bins=50) +
    geom_histogram(data=subset(uhi_data_std_date,city == 'parramatta'),fill = "green", alpha = 0.4, bins=50) +
    geom_histogram(data=subset(uhi_data_std_date,city == 'katoomba'),fill = "red", alpha = 0.4, bins=50) + 
  labs(title = "Temp.Min (Penrith, Parramatta & Katoomba)")


p8 = ggplot(uhi_data_std_date,aes(x=Temp.Min.std)) + 
    geom_histogram(data=subset(uhi_data_std_date,city == 'penrith'),fill = "blue", alpha = 0.4, bins=50) +
    geom_histogram(data=subset(uhi_data_std_date,city == 'parramatta'),fill = "green", alpha = 0.4, bins=50) +
    geom_histogram(data=subset(uhi_data_std_date,city == 'katoomba'),fill = "red", alpha = 0.4, bins=50) +
  labs(title = "Temp.Min.std (Penrith, Parramatta & Katoomba)") + 
    scale_fill_manual(name="City", values=c("blue","green", "red"),labels=c("penrith","parramatta", "katoomba"))


grid.arrange(p7, p8, nrow=2)
# -------------------------------------------------------------------------------


## Group the teperatures by year to see underlying trends
p9 = ggplot(uhi_data_std_date, aes(as.factor(Year), Temp.Max.std)) +
  geom_boxplot(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Max.std Vs Date (Penrith, Parramatta & Katoomba)")



p10 = ggplot(uhi_data_std_date, aes(as.factor(Year), Temp.Min.std)) +
  geom_boxplot(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Min.std Vs Date (Penrith, Parramatta & Katoomba)") +
  ylim(0, 4e-05)


grid.arrange(p9, p10, nrow=2)


## Group the teperatures by year to see underlying trends
p11 = ggplot(uhi_data_std_date, aes(as.factor(Year), Temp.Max)) +
  geom_boxplot(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Max Vs Date (Penrith, Parramatta & Katoomba)")



p12 = ggplot(uhi_data_std_date, aes(as.factor(Year), Temp.Min)) +
  geom_boxplot(aes(colour = factor(city)), size = 3) +
  labs(title = "Temp.Min Vs Date (Penrith, Parramatta & Katoomba)") 


grid.arrange(p11, p12, nrow=2)
```



## Remove seasonal component
```{r}


#install.packages("forecast")
#library(forecast)

uhi_pen = uhi_data_std_date[uhi_data_std_date$city == "penrith", ]
uhi_par = uhi_data_std_date[uhi_data_std_date$city == "parramatta", ]
uhi_kat = uhi_data_std_date[uhi_data_std_date$city == "katoomba", ]


## Penrith time series
ts_pen = ts(na.omit(uhi_pen$Temp.Max.std), frequency = 366)
decompose_pen = decompose(ts_pen, "multiplicative")
 
plot(as.ts(decompose_pen$seasonal))
plot(as.ts(decompose_pen$random))
plot(decompose_pen)


## Parramatta time series
ts_par = ts(na.omit(uhi_par$Temp.Max.std), frequency = 366)
decompose_par = decompose(ts_par, "multiplicative")
 
plot(as.ts(decompose_par$seasonal))
plot(as.ts(decompose_par$trend))
plot(as.ts(decompose_par$random))
plot(decompose_par)


## Katoomba time series
ts_kat = ts(na.omit(uhi_kat$Temp.Max.std), frequency = 366)
decompose_kat = decompose(ts_kat, "multiplicative")
 
plot(as.ts(decompose_kat$seasonal))
plot(as.ts(decompose_kat$trend))
plot(as.ts(decompose_kat$random))
plot(decompose_kat)

trends = cbind(decompose_pen$trend, decompose_par$trend, decompose_kat$trend) 
colnames(trends) = c("Penrith", "Parramatta", "Katoomba")
autoplot(ts( trends  , start = c(1,1), frequency = 366 ),
         facets = FALSE) +
  scale_size_manual(values = c(3, 3, 3))


## Penrith time series
ts_pen_min = ts(na.omit(uhi_pen$Temp.Min.std), frequency = 366)
decompose_pen_min = decompose(ts_pen_min, "multiplicative")
 
plot(as.ts(decompose_pen$seasonal))
plot(as.ts(decompose_pen_min$random))
plot(decompose_pen)


## Parramatta time series
ts_par_min = ts(na.omit(uhi_par$Temp.Min.std), frequency = 366)
decompose_par_min = decompose(ts_par_min, "multiplicative")
 
plot(as.ts(decompose_par$seasonal))
plot(as.ts(decompose_par_min$trend))
plot(as.ts(decompose_par$random))
plot(decompose_par)


## Katoomba time series
ts_kat_min = ts(na.omit(uhi_kat$Temp.Min.std), frequency = 366)
decompose_kat_min = decompose(ts_kat_min, "multiplicative")
 
plot(as.ts(decompose_kat$seasonal))
plot(as.ts(decompose_kat_min$trend))
plot(as.ts(decompose_kat$random))
plot(decompose_kat)


trends_min = cbind(decompose_pen_min$trend, decompose_par_min$trend, decompose_kat_min$trend) 
colnames(trends_min) = c("Penrith", "Parramatta", "Katoomba")
autoplot(ts( trends_min  , start = c(1,1), frequency = 366 ),
         facets = FALSE) +
  scale_size_manual(values = c(3, 3, 3))



```




## Correlations
```{r}

## All Cities

selected1 = select(uhi_data_std_date,
       "Date",
"Year",
"MonthDay",
"Month",
"Day",
"Temp.Min",
"Temp.Max",
"Precipitation",
"Solar.Radiation..MJ.m.m.",
"Available_Water_Capacity_value_100.200",
"Available_Water_Capacity_value_30.60",
"Available_Water_Capacity_value_60.100",
"Bulk_Density_value_0.5",
"Bulk_Density_value_15.30",
"Bulk_Density_value_30.60",
"Bulk_Density_value_5.15",
"Bulk_Density_value_60.100",
"Clay_value_0.5",
"Clay_value_100.200",
"Clay_value_15.30",
"Clay_value_5.15",
"Clay_value_60.100",
"Depth_to_Rock_value_0.5",
"ECEC_value_0.5",
"ECEC_value_100.200",
"ECEC_value_15.30",
"ECEC_value_30.60",
"ECEC_value_5.15",
"ECEC_value_60.100",
"Organic_Carbon_value_0.5",
"Organic_Carbon_value_15.30",
"Organic_Carbon_value_5.15",
"pH_Soil_CaCl2_value_0.5",
"pH_Soil_CaCl2_value_100.200",
"pH_Soil_CaCl2_value_15.30",
"pH_Soil_CaCl2_value_30.60",
"pH_Soil_CaCl2_value_5.15",
"pH_Soil_CaCl2_value_60.100",
"Total_Nitrogen_value_0.5",
"Total_Nitrogen_value_15.30",
"Total_Nitrogen_value_5.15",
"Separate.house",
"Semi.detached",
"Flat",
"Other",
"Not.Stated",
"Surface.Area",
"Population",
"cars",
"city",
"Separate.house.std",
"Semi.detached.std",
"Flat.std",
"Other.std",
"Not.Stated.std",
"Population.std",
"cars.std",
"sea.level",
"atmos",
"Temp.Max.std",
"Temp.Min.std")

selected1$sin = NA
selected1$sin = sin( ((2*pi/(365.25* 3 ))* rank(selected1$Date)) + (108) )

selected = select(selected1, -Date, -Year, Month, -Day, -city, -MonthDay)
#selected = cbind(selected, trends)
#str(selected)


selected$builtEnv = NA
selected$builtEnv = (selected$Separate.house + selected$Semi.detached + selected$Flat + selected$Not.Stated + selected$Other) / selected$Surface.Area



selected.std = scale(na.omit(selected))
cor.std = cor(selected.std)
cov.std = cov(selected.std)

cor.std[,c("sin", "Temp.Max", "Temp.Min", "Temp.Max.std", "Temp.Min.std", "Precipitation", "Solar.Radiation..MJ.m.m.")]
cov.std[,c("sin", "Temp.Max", "Temp.Min", "Temp.Max.std", "Temp.Min.std", "Precipitation", "Solar.Radiation..MJ.m.m.")]
```




# Model As Sinasoidal
As the temperature fluxuates with the season in a sinosaodal pattern, the function for a position of a wave will be incorporated into the model
```{r}
uhi_data_sin = uhi_data_std_date

## amp * sin((2*pi)/daysInAYeat)) + shift
#(max(uhi_data_sin$Temp.Max.std) - min(uhi_data_sin$Temp.Max.std))
uhi_data_sin$sin = (diff(quantile(uhi_data_sin$Temp.Max, c(0.1, 0.9), na.rm = T)) * sin( ((2*pi/(365.25*3))* rank(uhi_data_sin$Date)) + (108.5) )) + (mean(uhi_data_sin$Temp.Max) + 2)

plot(Temp.Max ~ as.Date(Date), data= uhi_data_sin)
lines(as.Date(uhi_data_sin$Date), uhi_data_sin$sin, col="blue", lwd=2)
```
Mapping all the temperatures to a sinasoidal pattern yeilds an interesting algorithm:
The amplitude of the above sinasoidal wave is the difference between the 10% & 90% quantiles of the maximum temperatures. The wave-length is expressed as 3 * 366 (3 years), the wave is shifted along the x axis by 108.5 days (3 and a half months) & shifted along the y-axis by the mean max temperature.

The above shows that by determining the amplitude of the wave relative to the x-axis (Date) a more precise prediction for the temperature can be obtained.

```{r}
ggplot(uhi_data_sin, aes(as.Date(Date), Temp.Max)) +
  geom_point(aes(colour = factor(city)), size = 3, alpha=0.4) +
  labs(title = "Temp.Max Vs Surface Area (Penrith, Parramatta & Katoomba)")


ggplot(uhi_data_sin, aes(as.Date(Date), Temp.Max.std)) +
  geom_point(aes(colour = factor(city)), size = 3, alpha=0.3) +
  labs(title = "Temp.Max.std Vs Surface Area (Penrith, Parramatta & Katoomba)")
```
(diff(quantile(uhi_data_sin$Temp.Max, c(0.15, 0.85), na.rm = T)) * sin( ((2*pi/(366*2))* rank(uhi_data_sin$Date)) + (31*3.5) )) + (median(uhi_data_sin$Temp.Max) + (0.0002177187*0.1) )

```{r}
uhi_pen_sin = uhi_data_sin[uhi_data_sin$city == "penrith", ]
uhi_pen_sin$sin = (diff(quantile(uhi_pen_sin$Temp.Max, c(0.1, 0.9), na.rm = T)) * sin( ((2*pi/(365.25*1))* rank(uhi_pen_sin$Date)) + (108.5) )) + (mean(uhi_pen_sin$Temp.Max) +2 )

#uhi_data_sin[is.na(uhi_data_sin$sin), ]

plot(Temp.Max ~ as.Date(Date), data= uhi_pen_sin)
lines(as.Date(uhi_pen_sin$Date), uhi_pen_sin$sin, col="blue", lwd=2)
```
Mapping the Penrith temperatures to a sinasoidal pattern yeilds an interesting algorithm:
The amplitude of the above sinasoidal wave is the difference between the 10% & 90% quantiles of the maximum temperatures. The wave-length is expressed as 366 (year), the wave is shifted along the x axis by 108.5 days (3 and a half months) & shifted along the y-axis by the mean max temperature (24.66963) plus 2 degrees


```{r}
uhi_par_sin = uhi_data_sin[uhi_data_sin$city == "parramatta", ]
uhi_par_sin$sin = (diff(quantile(uhi_par_sin$Temp.Max, c(0.1, 0.9), na.rm = T)) * sin( ((2*pi/(365.25*1.))* rank(uhi_par_sin$Date)) + (108.5) )) + (mean(uhi_par_sin$Temp.Max) +3)

#uhi_data_sin[is.na(uhi_data_sin$sin), ]

plot(Temp.Max ~ as.Date(Date), data= uhi_par_sin)
lines(as.Date(uhi_par_sin$Date), uhi_par_sin$sin, col="blue", lwd=2)
```
Mapping the Parramatta temperatures to a sinasoidal pattern yeilds an interesting algorithm:
The amplitude of the above sinasoidal wave is the difference between the 10% & 90% quantiles of the maximum temperatures. The wave-length is expressed as 366 (year), the wave is shifted along the x axis by 108.5 days (3 and a half months) & shifted along the y-axis by the mean max temperature (23.6936) plus 3 degrees


```{r}
uhi_kat_sin = uhi_data_sin[uhi_data_sin$city == "katoomba", ]
uhi_kat_sin$sin = (diff(quantile(uhi_kat_sin$Temp.Max.std, c(0.15, 0.85), na.rm = T)) * sin( ((2*pi/(365.25*1.))* rank(uhi_kat_sin$Date)) + (108.5) )) + (mean(uhi_kat_sin$Temp.Max.std))

#uhi_data_sin[is.na(uhi_data_sin$sin), ]

plot(Temp.Max.std ~ as.Date(Date), data= uhi_kat_sin)
lines(as.Date(uhi_kat_sin$Date), uhi_kat_sin$sin, col="blue", lwd=2)
```
Mapping the Katoomba temperatures to a sinasoidal pattern yeilds an interesting algorithm:
The amplitude of the above sinasoidal wave is the difference between the 10% & 90% quantiles of the maximum temperatures. The wave-length is expressed as 366 (year), the wave is shifted along the x axis by 108.5 days (3 and a half months) & shifted along the y-axis by the mean max temperature (17.58908) plus 3 degrees


The three varied shifts along the y-axis suggest a relative temperature medium dependent on the location (hight above sea-level) of the city. to account for this in the model the city will be encoded into a numeric variable so that the remaining variables can be modeled along with the ineteration of the city term.
```{r}
uhi_data_sin_city = uhi_data_sin
uhi_data_sin_city$cityTerm = NA

uhi_data_sin_city[uhi_data_sin_city$city == "penrith", "cityTerm"] = 1
uhi_data_sin_city[uhi_data_sin_city$city == "parramatta", "cityTerm"] = 2
uhi_data_sin_city[uhi_data_sin_city$city == "katoomba", "cityTerm"] = 3

## amp * sin((2*pi)/daysInAYeat)) + shift
uhi_data_sin_city$sin.std = 0
uhi_data_sin_city$sin.std = (diff(quantile(uhi_data_sin_city$Temp.Max.std, c(0.1, 0.9), na.rm = T)) * sin( ((2*pi/(365.25*3))* rank(uhi_data_sin_city$Date)) + (108.5) )) + (mean(uhi_data_sin_city$Temp.Max.std) )

#uhi_data_sin[is.na(uhi_data_sin$sin), ]

plot(Temp.Max.std ~ as.Date(Date), data = uhi_data_sin_city)
lines(as.Date(uhi_data_sin_city$Date), uhi_data_sin_city$sin.std, col="blue", lwd=2)

uhi_data_sin_city$sinTerm.std = sin( ((2*pi/(365.25*3))* rank(uhi_data_sin_city$Date)) + (108.5) ) 
uhi_data_sin_city$sinShiftTerm.std = (mean(uhi_data_sin_city$Temp.Max.std))


## amp * sin((2*pi)/daysInAYeat)) + shift
uhi_data_sin_city$sin = 0
uhi_data_sin_city$sin = (diff(quantile(uhi_data_sin_city$Temp.Max, c(0.1, 0.9), na.rm = T)) * sin( ((2*pi/(365.25*3))* rank(uhi_data_sin_city$Date)) + (108) )) + (mean(uhi_data_sin_city$Temp.Max) + ((2)))

#uhi_data_sin[is.na(uhi_data_sin$sin), ]

plot(Temp.Max ~ as.Date(Date), data = uhi_data_sin_city)
lines(as.Date(uhi_data_sin_city$Date), uhi_data_sin_city$sin, col="blue", lwd=2)

## Plot sinasoidal
# --------------------------------------------------------
for (i in seq(0.5, 2.5, 0.5)) {
  print( i)
  
  uhi_data_sin_city$sin = (diff(quantile(uhi_data_sin_city$Temp.Max, c(0.1, 0.9), na.rm = T)) * sin( ((2*pi/(366*3))* rank(uhi_data_sin_city$Date)) + (107.5 + i) )) + (mean(uhi_data_sin_city$Temp.Max) + ((2)))
  
  p = ggplot(uhi_data_sin_city, aes(as.Date(Date), Temp.Max)) +
  geom_point(aes(colour = factor(city)), size = 3) +
  geom_line(aes(x = as.Date(Date), y = sin), color = "blue", size = 1.5) + 
  labs(title = paste("Temp Max Vs Date (Penrith, Parramatta & Katoomba) (", i, ")"))
  
  p2 = ggplot(uhi_data_sin_city, aes(as.Date(Date), Temp.Max.std)) +
  geom_point(aes(colour = factor(city)), size = 2) +
  geom_line(aes(x = as.Date(Date), y = sin.std), color = "blue", size = 1.5) +
  labs(title = paste("Temp Max STD Vs Date (Penrith, Parramatta & Katoomba) (", i, ")"))
  
  print(p)
}


p1 = ggplot(uhi_data_sin_city, aes(as.Date(Date), Temp.Max)) +
  geom_point(aes(colour = factor(city)), size = 2) +
  geom_line(aes(x = as.Date(Date), y = sin), color = "blue", size = 1.5) + 
  labs(title = "Temp Max Vs Date (Penrith, Parramatta & Katoomba)")

p2 = ggplot(uhi_data_sin_city, aes(as.Date(Date), Temp.Max.std)) +
  geom_point(aes(colour = factor(city)), size = 2) +
  geom_line(aes(x = as.Date(Date), y = sin.std), color = "blue", size = 1.5) + 
  labs(title = "Temp Max STD Vs Date (Penrith, Parramatta & Katoomba)")

p1
p1
p2

grid.arrange(p1, p2, nrow=2)
# --------------------------------------------------------



# The Sin Term is made up of the sin function of ( ((2 * pi) over the wavelength) multiplied by how far along x
uhi_data_sin_city$sinTerm = sin( ((2*pi/(365.25* length(unique(env.test_all$city)) ))* rank(uhi_data_sin_city$Date)) + (108) ) 
uhi_data_sin_city$sinShiftTerm = (mean(uhi_data_sin_city$Temp.Max) + ((1+uhi_data_sin_city$cityTerm)))




```


```{r}
# Set seed to replicate results & split data into test and training sets
set.seed(101)
train_all = sample(1:nrow(uhi_data_sin_city), nrow(uhi_data_sin_city)/2)
env.train_all <- uhi_data_sin_city[train_all, ]
env.test_all <- uhi_data_sin_city[-train_all, ]

env.train_all$Date = as.Date(env.train_all$Date)
env.test_all$Date = as.Date(env.test_all$Date)
```


### Models
```{r}
model.5.1 = lm(Temp.Max.std ~ 
                 Available_Water_Capacity_value_0.5 + 
                 (Population.std / cars.std) +
                 (Solar.Radiation..MJ.m.m. / Bulk_Density_value_0.5) + 
                 (Organic_Carbon_value_0.5) +
                 (Month / Depth_to_Rock_value_0.5)  +
                 (Solar.Radiation..MJ.m.m. * sinTerm) + 
                 (sinTerm * Precipitation) +
                 (sinTerm / builtEnv) + 
                 (sinTerm / Population.std) + 
                 (sinTerm * Month)
                 , env.train_all)
summary(model.5.1)
# find predictions for original time series
plot(model.5.1)

pred.5.1 <- predict(model.5.1, env.test_all)    

plot(Temp.Max.std ~ as.numeric(Date), data= env.test_all)
lines(as.numeric(env.test_all$Date), pred.5.1, col="blue", lwd=2)

plot(Temp.Max.std ~ pred.5.1, data= env.test_all)
lines(env.test_all$Temp.Max.std, env.test_all$Temp.Max.std, col="blue", lwd=2)


model.5.2 = lm(Temp.Max ~ 
                 Available_Water_Capacity_value_0.5 + 
                 (Population.std / cars.std) +
                 (Solar.Radiation..MJ.m.m. / Bulk_Density_value_0.5) + 
                 (Organic_Carbon_value_0.5) +
                 (Month / Depth_to_Rock_value_0.5)  +
                 (Solar.Radiation..MJ.m.m. * sinTerm) + 
                 (sinTerm * Precipitation) +
                 (sinTerm / builtEnv) + 
                 (sinTerm / Population.std) + 
                 (sinTerm * Month)
                 , env.train_all)
summary(model.5.2)



# find predictions for original time series
plot(model.5.2)

pred.5.2 <- predict(model.5.2, env.test_all)    

plot(Temp.Max ~ as.numeric(Date), data= env.test_all)
lines(as.numeric(env.test_all$Date), pred.5.2, col="blue", lwd=2)

plot(Temp.Max ~ pred.5.2, data= env.test_all)
lines(env.test_all$Temp.Max, env.test_all$Temp.Max, col="blue", lwd=2)
```
