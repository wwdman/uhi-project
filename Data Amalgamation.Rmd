---
title: "Data Amalgamation"
author: "Daniel Stratti 18577857"
date: "16/09/2019"
output: word_document
---

```{r setup, include=FALSE}
# Install packages
#install.packages('dplyr')
#install.packages('tidyr')
#install.packages("ggplot2")

# Import Libraries
library(dplyr)
library(tidyr)
library(ggplot2)
library(gridExtra)
```

# Data Amalgamate
Data will be amaligimated from three different sources, the Bureau of Meteorology (BOM), the CSIRO Soil and Landscape Grid of Australia (SLGA) and the AUSTRALIAN BUREAU OF STATISTICS (ABS) Census from 1991 - 2016. These three data sources will be used to source data for the areas of Penrith, Parramatta & the Blue Mountains.

------------------------------------------------------------------------------------------------------
## Amalgamate Penrith Data
------------------------------------------------------------------------------------------------------

#### Amalgamate BOM Data
------------------------------------------------------------------------------------------------------
```{r}
# Read in BOM data for temp (Max & Min), precipitation and solar radiation
temp_penrith_max = read.csv("./datasets/BOM/IDCJAC0010_067113_1800_Data.csv")
temp_penrith_min = read.csv("./datasets/BOM/IDCJAC0011_067113_1800_Data.csv")
solar_penrith = read.csv("./datasets/BOM/IDCJAC0016_067113_1800_Data_2.csv")
rain_penrith = read.csv("./datasets/BOM/IDCJAC0009_067113_1800_Data.csv")

nrow(temp_penrith_max)
nrow(temp_penrith_min)
nrow(solar_penrith)
nrow(rain_penrith)

### Rearrange data
temp_min = unite_(temp_penrith_min, "date", c("Year", "Month", "Day"), '-') 
temp_min$date = as.Date(temp_min$date)
summary(temp_min)

temp_max = unite_(temp_penrith_max, "date", c("Year", "Month", "Day"), '-') 
temp_max$date = as.Date(temp_max$date)
summary(temp_max)

solar = unite_(solar_penrith, "date", c("Year", "Month", "Day"), '-') 
solar$date = as.Date(solar$date)
summary(solar)

rain = unite_(rain_penrith, "date", c("Year", "Month", "Day"), '-')
rain$date = as.Date(rain$date)
summary(rain)


# Set amalgData$Date to be from 1995-01-01 -- 2019-01-01
amalgData = data.frame(
  "Date"=seq.Date(from = as.Date('1995-01-01'), to = as.Date('2019-01-01'), by = 'days'))

amalgData$Date = as.Date(amalgData$Date)

amalgData[, "Temp Min"] = NA
amalgData[, "Temp Max"] = NA
amalgData[, "Precipitation"] = NA
amalgData[, "Solar Radiation (MJ.m.m)"] = NA

head(amalgData)

class(amalgData$Date)
class(solar$date)

for (current_date in amalgData$Date) {
  #print(current_date)
  
  amalgData[amalgData$Date == current_date, "Temp Min"] = temp_min[temp_min$date == current_date, "temp"]
  
  amalgData[amalgData$Date == current_date, "Temp Max"] = temp_max[temp_max$date == current_date, "temp"]
  
  amalgData[amalgData$Date == current_date, "Precipitation"] = rain[rain$date == current_date, "Rainfall.amount..millimetres."]
  
  amalgData[amalgData$Date == current_date, "Solar Radiation (MJ.m.m)"] = solar[solar$date == current_date, "Daily.global.solar.exposure..MJ.m.m."]
}

summary(amalgData)

write.csv(amalgData, "penrith/bom.csv", row.names = F)
```

The data collected from the BOM was transformed to join the year, month and day into a single unique date column. The Min, Max temperatures were then amalgamated with the precipitation and solar radiance data.

#### Amalgamate CSIRO SLGA Data
------------------------------------------------------------------------------------------------------
```{r}
weather_table = read.csv("penrith/bom.csv")
summary(weather_table)
slga_data = read.csv("penrith/penrith_slga_data.csv")
summary(slga_data)
slga_amalg = data.frame(weather_table, slga_data[,2:ncol(slga_data)])
slga_amalg$Date = as.Date(slga_amalg$Date)
summary(slga_amalg)


write.csv(slga_amalg, "penrith/bom_and_slga.csv", row.names = F)

```

THe SLGA data was extraced from .tif files collected from the SLGA API. The various soil attributes will be explored and locations compared to see if there is any relationship between the likely hood of increase in tempreture and the soil conditions of that area.

#### Amalgamate ABS Data
------------------------------------------------------------------------------------------------------
```{r}
# Read in ABS Data
## Totals from T14 DWELLING STRUCTURE BY HOUSEHOLD COMPOSITION AND FAMILY COMPOSITION 2016
census_2016 = data.frame(
  "Date" = c("2006-04-11", "2011-04-11", "2016-04-11"),
  "Separate house" = c(49943, 49817, 54018),
  "Semi-detached" = c(4888, 6717, 8072),
  "Flat" = c(3679, 4575, 4402),
  "Other" = c(377, 292, 314),
  "Not Stated" = c(28, 103, 255),
  "Surface Area" = c(404.7, 404.7, 404.7),
  "Population" = c(171563, 177984, 195301),
  "cars" = c(94506, 103350, 115490)
)
# Population data from https://profile.id.com.au/

## T14 DWELLING STRUCTURE BY HOUSEHOLD COMPOSITION(a) AND FAMILY COMPOSITION(b)
census_2011 = data.frame(
  "Date" = c("2001-08-09", "2006-08-09", "2011-08-09"),
  "Separate house" = c(48813, 49942, 49817),
  "Semi-detached" = c(3990, 4887, 6717),
  "Flat" = c(3680, 3679, 4573),
  "Other" = c(261, 375, 292),
  "Not Stated" = c(507, 29, 102),
  "Surface Area" = c(404.6, 404.6, 404.6),
  "Population" = c(171870, 171563, 177984),
  "cars" = c(85390, 94506, 103350)
)

## T14 DWELLING STRUCTURE BY HOUSEHOLD COMPOSITION(a) AND FAMILY COMPOSITION(b) FOR TIME SERIES
census_2006 = data.frame(
  "Date" = c("1996-08-08", "2001-08-08", "2006-08-08"),
  "Separate house" = c(45327, 48813, 49942),
  "Semi-detached" = c(2598, 3990, 4887),
  "Flat" = c(3321, 3677, 3679),
  "Other" = c(357, 255, 375),
  "Not Stated" = c(960, 507, 28),
  "Surface Area" = c(404.9, 404.9, 404.9),
  "Population" = c(162762, 171870, 171563),
  "cars" = c(76515, 85390, 94506)
)
 
## T18  DWELLING STRUCTURE
census_2001 = data.frame(
  "Date" = c("1991-08-07", "1996-08-07", "2001-08-07"),
  "Separate house" = c(42426, 46984, 50633),
  "Semi-detached" = c(1647, 2821, 4413),
  "Flat" = c(3342, 3663, 4048),
  "Other" = c(398, 373, 283),
  "Not Stated" = c(333, 1144, 571),
  "Surface Area" = c(404.8, 404.8, 404.8),
  "Population" = c(149696, 162762, 171870),
  "cars" = c(70000, 76515, 85390)
)

## Add data to new amalgamated dataset
censusData = rbind(census_2001, census_2006, census_2011, census_2016)
censusData = censusData[order(as.Date(censusData$Date)), ]
censusData$Date = as.Date(censusData$Date)

## Amalgamate census data to rest of data

tail(slga_amalg$Date)
census_amalg = slga_amalg
census_amalg$Separate.house = NA
census_amalg$Semi.detached = NA
census_amalg$Flat = NA
census_amalg$Other = NA 
census_amalg$Not.Stated = NA
census_amalg$Surface.Area = NA
census_amalg$Population = NA
census_amalg$cars = NA


census_amalg$Date = as.Date(census_amalg$Date)
for (date in (censusData$Date)) {
  #Separate.house Semi.detached Flat Other Not.Stated Surface.Area
  #print(date)
  census_amalg[census_amalg$Date == date, "Separate.house"] = censusData[censusData$Date == date, "Separate.house"]
  census_amalg[census_amalg$Date == date, "Semi.detached"] = censusData[censusData$Date == date, "Semi.detached"]
  census_amalg[census_amalg$Date == date, "Flat"] = censusData[censusData$Date == date, "Flat"]
  census_amalg[census_amalg$Date == date, "Other"] = censusData[censusData$Date == date, "Other"]
  census_amalg[census_amalg$Date == date, "Not.Stated"] = censusData[censusData$Date == date, "Not.Stated"]
  census_amalg[census_amalg$Date == date, "Surface.Area"] = censusData[censusData$Date == date, "Surface.Area"]
  census_amalg[census_amalg$Date == date, "Population"] = censusData[censusData$Date == date, "Population"]
  census_amalg[census_amalg$Date == date, "cars"] = censusData[censusData$Date == date, "cars"]
}

censusData$Date = as.Date(censusData$Date)
census_amalg$Date = as.Date(census_amalg$Date)
summary(census_amalg)

ggplot(data = census_amalg, aes(x = Date, y = cars)) +
  geom_point() +
  geom_smooth(method = 'gam') +
  labs(title = "Census Housing data (Seperated houses)")
```

### Smooth & Predicted values for the missing census data
------------------------------------------------------------------------------------------------------
```{r}
write.csv(census_amalg, "penrith/bom_slga_census.csv", row.names = F)

## Use the loess function to smooth out predicted values for the missing census data
### Seperate houses
pen_seph_smooth = loess(Separate.house ~ as.numeric(Date), censusData, span = 0.5)
census_amalg$Separate.house = predict(pen_seph_smooth, as.Date(census_amalg$Date))

### Semidetached houses
pen_semid_smooth = loess(Semi.detached ~ as.numeric(Date), censusData, span = 0.5)
census_amalg$Semi.detached = predict(pen_semid_smooth, as.Date(census_amalg$Date))

### Flats
pen_flats_smooth = loess(Flat ~ as.numeric(Date), censusData, span = 0.5)
census_amalg$Flat = predict(pen_flats_smooth, as.Date(census_amalg$Date))

### Other houses
pen_other_smooth = loess(Other ~ as.numeric(Date), censusData, span = 0.5)
census_amalg$Other = predict(pen_other_smooth, as.Date(census_amalg$Date))

### Not Stated
pen_ns_smooth = loess(Not.Stated ~ as.numeric(Date), censusData, span = 0.5)
census_amalg$Not.Stated = predict(pen_ns_smooth, as.Date(census_amalg$Date))

### Surface Area
pen_sa_smooth = loess(Surface.Area ~ as.numeric(Date), censusData, span = 0.5)
census_amalg$Surface.Area = predict(pen_sa_smooth, as.numeric(census_amalg$Date))

### Population
pen_pop_smooth = loess(Population ~ as.numeric(Date), censusData, span = 0.5)
census_amalg$Population = predict(pen_pop_smooth, as.numeric(census_amalg$Date))

### cars
pen_car_smooth = loess(cars ~ as.numeric(Date), censusData, span = 0.5)
census_amalg$cars = predict(pen_car_smooth, as.numeric(census_amalg$Date))


write.csv(census_amalg, "penrith/bom_slga_census_sm.csv", row.names = F)
summary(census_amalg)

census_amalg[is.na(census_amalg$Surface.Area), ]

ggplot(data = census_amalg, aes(x = Date, y = Surface.Area)) +
  geom_point() +
  geom_smooth(method = 'gam') +
  labs(title = "Census Housing data (Seperated houses)")
```



------------------------------------------------------------------------------------------------------
## Amalgamate Parramatta Data
------------------------------------------------------------------------------------------------------

### Amalgamate B.O.M Data
------------------------------------------------------------------------------------------------------
```{r}
# Read in BOM data for temp (Max & Min), precipitation and solar radiation
temp_parra_max = read.csv("./datasets/BOM/IDCJAC0010_066124_1800_Data.csv")
temp_parra_min = read.csv("./datasets/BOM/IDCJAC0011_066124_1800_Data.csv")
solar_parra = read.csv("./datasets/BOM/IDCJAC0016_066124_1800_Data.csv")
rain_parra = read.csv("./datasets/BOM/IDCJAC0009_066124_1800_Data.csv")

nrow(temp_parra_max)
nrow(temp_parra_min)
nrow(solar_parra)
nrow(rain_parra)

### Rearrange data
temp_min = unite_(temp_parra_min, "date", c("Year", "Month", "Day"), '-') 
temp_min$date = as.Date(temp_min$date)
summary(temp_min)

temp_max = unite_(temp_parra_max, "date", c("Year", "Month", "Day"), '-') 
temp_max$date = as.Date(temp_max$date)
summary(temp_max)

solar = unite_(solar_parra, "date", c("Year", "Month", "Day"), '-') 
solar$date = as.Date(solar$date)
summary(solar)

rain = unite_(rain_parra, "date", c("Year", "Month", "Day"), '-')
rain$date = as.Date(rain$date)
summary(rain)

# Set amalgData$Date to be from 1995-01-01 -- 2019-01-01
amalgData = data.frame(
  "Date"=seq.Date(from = as.Date('1995-01-01'), to = as.Date('2019-01-01'), by = 'days'))

amalgData$Date = as.Date(amalgData$Date)

amalgData[, "Temp Min"] = NA
amalgData[, "Temp Max"] = NA
amalgData[, "Precipitation"] = NA
amalgData[, "Solar Radiation (MJ.m.m)"] = NA

class(amalgData$Date)
class(temp_max$date)

for (current_date in amalgData$Date) {
  #print(current_date)
  
  if (sum(temp_min$date == current_date)) {
    amalgData[amalgData$Date == current_date, "Temp Min"] = temp_min[temp_min$date == current_date, "Minimum.temperature..Degree.C."]
  }
  
  if (sum(temp_max$date == current_date)) {
    amalgData[amalgData$Date == current_date, "Temp Max"] = temp_max[temp_max$date == current_date, "Maximum.temperature..Degree.C."]
  }
  
  if (sum(rain$date == current_date)) {
    amalgData[amalgData$Date == current_date, "Precipitation"] = rain[rain$date == current_date, "Rainfall.amount..millimetres."]
  }
  
  if (sum(solar$date == current_date)) {
    amalgData[amalgData$Date == current_date, "Solar Radiation (MJ.m.m)"] = solar[solar$date == current_date, "Daily.global.solar.exposure..MJ.m.m."]
  }
}

summary(amalgData)

write.csv(amalgData, "parra/bom.csv", row.names = F)
```

### Amalgamate CSIRO SLGA Data
------------------------------------------------------------------------------------------------------
```{r}
weather_table = read.csv("parra/bom.csv")
summary(weather_table)

slga_data = read.csv("./parra/parra_slga_data.csv")
summary(slga_data)

slga_amalg = data.frame(weather_table, slga_data[,2:ncol(slga_data)])
slga_amalg$Date = as.Date(slga_amalg$Date)
summary(slga_amalg)
write.csv(slga_amalg, "parra/bom_and_slga.csv", row.names = F)
```


### Amalgamate Parramatta A.B.S Census Data
------------------------------------------------------------------------------------------------------
```{r}
# Read in ABS Data
## Totals from T14 DWELLING STRUCTURE BY HOUSEHOLD COMPOSITION AND FAMILY COMPOSITION 2016
census_2016 = data.frame(
  "Date" = c("2016-04-11"),
  "Separate house" = c(1119),
  "Semi-detached" = c(787),
  "Flat" = c(2793),
  "Other" = c(5),
  "Not Stated" = c(25),
  "Surface Area" = c(5.2),
  "Population" = c(13390),
  "cars" = c(6045)
)
# Population data from https://profile.id.com.au/

## T14 DWELLING STRUCTURE BY HOUSEHOLD COMPOSITION(a) AND FAMILY COMPOSITION(b)
census_2011 = data.frame(
  "Date" = c("2011-08-09"),
  "Separate house" = c(1179),
  "Semi-detached" = c(788),
  "Flat" = c(2896),
  "Other" = c(0),
  "Not Stated" = c(12),
  "Surface Area" = c(5.5),
  "Population" = c(13006),
  "cars" = c(5593)
)


# Population data from https://profile.id.com.au/

## T14 DWELLING STRUCTURE BY HOUSEHOLD COMPOSITION(a) AND FAMILY COMPOSITION(b) FOR TIME SERIES
census_2006 = data.frame(
  "Date" = c("2006-08-08"),
  "Separate house" = c(733),
  "Semi-detached" = c(223),
  "Flat" = c(2568),
  "Other" = c(10),
  "Not Stated" = c(0),
  "Surface Area" = c(3.1),
  "Population" = c(10404),
  "cars" = c(3474)
)
# Population data from https://profile.id.com.au/
 
## T18  DWELLING STRUCTURE
census_2001 = data.frame(
  "Date" = c("2001-08-07"),
  "Separate house" = c(889),
  "Semi-detached" = c(164),
  "Flat" = c(2271),
  "Other" = c(18),
  "Not Stated" = c(298),
  "Surface Area" = c(4.4),
  "Population" = c(10119),
  "cars" = c(3426)
)



census_1996 = data.frame(
  "Date" = c("1996-08-07"),
  "Separate house" = c(93879),
  "Semi-detached" = c(9563),
  "Flat" = c(25782),
  "Other" = c(551),
  "Not Stated" = c(3216),
  "Surface Area" = c(60.95),
  "Population" = c(8275),
  "cars" = c(59894)
)
# Population data from https://profile.id.com.au/

## Add data to new amalgamated dataset
censusData = rbind(census_1996, census_2001, census_2006, census_2011, census_2016)
censusData = censusData[order(as.Date(censusData$Date)), ]
censusData$Date = as.Date(censusData$Date)

## Amalgamate census data to rest of data

tail(slga_amalg$Date)
census_amalg = slga_amalg
census_amalg$Separate.house = NA
census_amalg$Semi.detached = NA
census_amalg$Flat = NA
census_amalg$Other = NA 
census_amalg$Not.Stated = NA
census_amalg$Surface.Area = NA
census_amalg$Population = NA
census_amalg$cars = NA

census_amalg$Date = as.Date(census_amalg$Date)
for (date in (censusData$Date)) {
  #Separate.house Semi.detached Flat Other Not.Stated Surface.Area
  print(date)
  census_amalg[census_amalg$Date >= date, "Separate.house"] = censusData[censusData$Date == date, "Separate.house"]
  census_amalg[census_amalg$Date >= date, "Semi.detached"] = censusData[censusData$Date == date, "Semi.detached"]
  census_amalg[census_amalg$Date >= date, "Flat"] = censusData[censusData$Date == date, "Flat"]
  census_amalg[census_amalg$Date >= date, "Other"] = censusData[censusData$Date == date, "Other"]
  census_amalg[census_amalg$Date >= date, "Not.Stated"] = censusData[censusData$Date == date, "Not.Stated"]
  census_amalg[census_amalg$Date >= date, "Surface.Area"] = censusData[censusData$Date == date, "Surface.Area"]
  census_amalg[census_amalg$Date >= date, "Population"] = censusData[censusData$Date == date, "Population"]
  census_amalg[census_amalg$Date >= date, "cars"] = censusData[censusData$Date == date, "cars"]
}

censusData$Date = as.Date(censusData$Date)
census_amalg$Date = as.Date(census_amalg$Date)
summary(census_amalg)

ggplot(data = census_amalg, aes(x = Date, y = cars)) +
  geom_point() +
  geom_smooth(method = 'gam') +
  labs(title = "Census Housing data (Seperated houses)")
```

#### Smooth & Predicted values for the missing Parramatta census data
------------------------------------------------------------------------------------------------------
```{r}
write.csv(census_amalg, "parra/bom_slga_census.csv", row.names = F)

## Use the loess function to smooth out predicted values for the missing census data
### Seperate houses
parra_seph_smooth = loess(Separate.house ~ as.numeric(Date), data = census_amalg, span = 0.5)
census_amalg$Separate.house = predict(parra_seph_smooth, as.Date(census_amalg$Date))

### Semidetached houses
parra_semid_smooth = loess(Semi.detached ~ as.numeric(Date), data = census_amalg, span = 0.5)
census_amalg$Semi.detached = predict(parra_semid_smooth, as.Date(census_amalg$Date))

### Flats
parra_flats_smooth = loess(Flat ~ as.numeric(Date), data = census_amalg, span = 0.5)
census_amalg$Flat = predict(parra_flats_smooth, as.Date(census_amalg$Date))

### Other houses
parra_other_smooth = loess(Other ~ as.numeric(Date), data = census_amalg, span = 0.5)
census_amalg$Other = predict(parra_other_smooth, as.Date(census_amalg$Date))

### Not Stated
parra_ns_smooth = loess(Not.Stated ~ as.numeric(Date), data = census_amalg, span = 0.5)
census_amalg$Not.Stated = predict(parra_ns_smooth, as.Date(census_amalg$Date))

### Surface Area
parra_sa_smooth = loess(Surface.Area ~ as.numeric(Date), data = census_amalg, span = 0.2)
census_amalg$Surface.Area = predict(parra_sa_smooth, as.numeric(census_amalg$Date))

### Population
parra_pop_smooth = loess(Population ~ as.numeric(Date), censusData, span = 0.5)
census_amalg$Population = predict(parra_pop_smooth, as.numeric(census_amalg$Date))


### cars
parra_car_smooth = loess(cars ~ as.numeric(Date), censusData, span = 0.5)
census_amalg$cars = predict(parra_car_smooth, as.numeric(census_amalg$Date))

ggplot(data = census_amalg, aes(x = Date, y = Surface.Area)) +
  geom_point() +
  geom_smooth(method = 'gam') +
  labs(title = "Census Housing data (Seperated houses)")

min(na.omit(census_amalg$Surface.Area))

write.csv(census_amalg, "parra/bom_slga_census_sm.csv", row.names = F)
summary(census_amalg)

census_amalg[is.na(census_amalg$Surface.Area), ]
```



------------------------------------------------------------------------------------------------------
## Amalgamate Blue Mountains (Katoomba) Data
------------------------------------------------------------------------------------------------------

### Amalgamate B.O.M Data
------------------------------------------------------------------------------------------------------
```{r}
# Read in BOM data for temp (Max & Min), precipitation and solar radiation
temp_katoo_max = read.csv("./datasets/BOM/IDCJAC0010_063039_1800_Data.csv")
temp_katoo_min = read.csv("./datasets/BOM/IDCJAC0011_063039_1800_Data.csv")
solar_katoo = read.csv("./datasets/BOM/IDCJAC0016_063039_1800_Data.csv")
rain_katoo = read.csv("./datasets/BOM/IDCJAC0009_063039_1800_Data.csv")

nrow(temp_katoo_max)
nrow(temp_katoo_min)
nrow(solar_katoo)
nrow(rain_katoo)

### Rearrange data
temp_min = unite_(temp_katoo_min, "date", c("Year", "Month", "Day"), '-') 
temp_min$date = as.Date(temp_min$date)
summary(temp_min)

temp_max = unite_(temp_katoo_max, "date", c("Year", "Month", "Day"), '-') 
temp_max$date = as.Date(temp_max$date)
summary(temp_max)

solar = unite_(solar_katoo, "date", c("Year", "Month", "Day"), '-') 
solar$date = as.Date(solar$date)
summary(solar)

rain = unite_(rain_katoo, "date", c("Year", "Month", "Day"), '-')
rain$date = as.Date(rain$date)
summary(rain)

# Set amalgData$Date to be from 1995-01-01 -- 2019-01-01
amalgData = data.frame(
  "Date"=seq.Date(from = as.Date('1995-01-01'), to = as.Date('2019-01-01'), by = 'days'))

amalgData$Date = as.Date(amalgData$Date)

amalgData[, "Temp Min"] = NA
amalgData[, "Temp Max"] = NA
amalgData[, "Precipitation"] = NA
amalgData[, "Solar Radiation (MJ.m.m)"] = NA

class(amalgData$Date)
class(temp_max$date)

for (current_date in amalgData$Date) {
  #print(current_date)
  
  if (sum(temp_min$date == current_date)) {
    amalgData[amalgData$Date == current_date, "Temp Min"] = temp_min[temp_min$date == current_date, "Minimum.temperature..Degree.C."]
  }
  
  if (sum(temp_max$date == current_date)) {
    amalgData[amalgData$Date == current_date, "Temp Max"] = temp_max[temp_max$date == current_date, "Maximum.temperature..Degree.C."]
  }
  
  if (sum(rain$date == current_date)) {
    amalgData[amalgData$Date == current_date, "Precipitation"] = rain[rain$date == current_date, "Rainfall.amount..millimetres."]
  }
  
  if (sum(solar$date == current_date)) {
    amalgData[amalgData$Date == current_date, "Solar Radiation (MJ.m.m)"] = solar[solar$date == current_date, "Daily.global.solar.exposure..MJ.m.m."]
  }
}

summary(amalgData)

write.csv(amalgData, "katoomba/bom.csv", row.names = F)
```

### Amalgamate CSIRO SLGA Data
------------------------------------------------------------------------------------------------------
```{r}
weather_table = read.csv("katoomba/bom.csv")
summary(weather_table)
slga_data = read.csv("katoomba/katoomba_slga_data.csv")
summary(slga_data)
slga_amalg = data.frame(weather_table, slga_data[,2:ncol(slga_data)])
slga_amalg$Date = as.Date(slga_amalg$Date)
summary(slga_amalg)


write.csv(slga_amalg, "katoomba/bom_and_slga.csv", row.names = F)

```

### Amalgamate A.B.S Katoomba Census Data
------------------------------------------------------------------------------------------------------
```{r}
# Read in ABS Data
## Totals from T14 DWELLING STRUCTURE BY HOUSEHOLD COMPOSITION AND FAMILY COMPOSITION 2016
census_2016 = data.frame(
  "Date" = c("2016-04-11"),
  "Separate house" = c(2812),
  "Semi-detached" = c(194),
  "Flat" = c(346),
  "Other" = c(22),
  "Not Stated" = c(19),
  "Surface Area" = c(22.1),
  "Population" = c(8887),
  "cars" = c(4237)
)
# Population data from https://profile.id.com.au/

## T14 DWELLING STRUCTURE BY HOUSEHOLD COMPOSITION(a) AND FAMILY COMPOSITION(b)
census_2011 = data.frame(
  "Date" = c("2011-08-09"),
  "Separate house" = c(2810),
  "Semi-detached" = c(227),
  "Flat" = c(258),
  "Other" = c(13),
  "Not Stated" = c(9),
  "Surface Area" = c(19.4),
  "Population" = c(8802),
  "cars" = c(3893)
)
# Population data from https://profile.id.com.au/

## T14 DWELLING STRUCTURE BY HOUSEHOLD COMPOSITION(a) AND FAMILY COMPOSITION(b) FOR TIME SERIES
census_2006 = data.frame(
  "Date" = c("2006-08-08"),
  "Separate house" = c(2706),
  "Semi-detached" = c(132),
  "Flat" = c(273),
  "Other" = c(16),
  "Not Stated" = c(0),
  "Surface Area" = c(21.9),
  "Population" = c(8456),
  "cars" = c(3471)
)
# Population data from https://profile.id.com.au/
 
## T18  DWELLING STRUCTURE
census_2001 = data.frame(
  "Date" = c("2001-08-07"),
  "Separate house" = c(6455),
  "Semi-detached" = c(291),
  "Flat" = c(520),
  "Other" = c(49),
  "Not Stated" = c(1892),
  "Surface Area" = c(73.8),
  "Population" = c(8737),
  "cars" = c(8192)
)

census_1996 = data.frame(
  "Date" = c("1996-08-07"),
  "Separate house" = c(65769),
  "Semi-detached" = c(1108),
  "Flat" = c(1436),
  "Other" = c(280),
  "Not Stated" = c(1207),
  "Surface Area" = c(1431.1),
  "Population" = c(8275),
  "cars" = c(34071)
)
# Population data from https://profile.id.com.au/

## Add data to new amalgamated dataset
censusData = rbind(census_1996, census_2001, census_2006, census_2011, census_2016)
censusData = censusData[order(as.Date(censusData$Date)), ]
censusData$Date = as.Date(censusData$Date)

## Amalgamate census data to rest of data

tail(slga_amalg$Date)
census_amalg = slga_amalg
census_amalg$Separate.house = NA
census_amalg$Semi.detached = NA
census_amalg$Flat = NA
census_amalg$Other = NA 
census_amalg$Not.Stated = NA
census_amalg$Surface.Area = NA
census_amalg$Population = NA
census_amalg$cars = NA

census_amalg$Date = as.Date(census_amalg$Date)
for (date in (censusData$Date)) {
  #Separate.house Semi.detached Flat Other Not.Stated Surface.Area
  #print(date)
  census_amalg[census_amalg$Date >= date, "Separate.house"] = censusData[censusData$Date == date, "Separate.house"]
  census_amalg[census_amalg$Date >= date, "Semi.detached"] = censusData[censusData$Date == date, "Semi.detached"]
  census_amalg[census_amalg$Date >= date, "Flat"] = censusData[censusData$Date == date, "Flat"]
  census_amalg[census_amalg$Date >= date, "Other"] = censusData[censusData$Date == date, "Other"]
  census_amalg[census_amalg$Date >= date, "Not.Stated"] = censusData[censusData$Date == date, "Not.Stated"]
  census_amalg[census_amalg$Date >= date, "Surface.Area"] = censusData[censusData$Date == date, "Surface.Area"]
  census_amalg[census_amalg$Date >= date, "Population"] = censusData[censusData$Date == date, "Population"]
  census_amalg[census_amalg$Date >= date, "cars"] = censusData[censusData$Date == date, "cars"]
}

censusData$Date = as.Date(censusData$Date)
census_amalg$Date = as.Date(census_amalg$Date)
summary(census_amalg)

ggplot(data = census_amalg, aes(x = Date, y = cars)) +
  geom_point() +
  geom_smooth(method = 'gam') +
  labs(title = "Census Housing data (Seperated houses)")
```


#### Smooth & Predicted values for the missing census data
------------------------------------------------------------------------------------------------------
```{r}
write.csv(census_amalg, "katoomba/bom_slga_census.csv", row.names = F)

## Use the loess function to smooth out predicted values for the missing census data
### Seperate houses
parra_seph_smooth = loess(Separate.house ~ as.numeric(Date), data = census_amalg, span = 0.5)
census_amalg$Separate.house = predict(parra_seph_smooth, as.Date(census_amalg$Date))



### Semidetached houses
parra_semid_smooth = loess(Semi.detached ~ as.numeric(Date), data = census_amalg, span = 0.5)
census_amalg$Semi.detached = predict(parra_semid_smooth, as.Date(census_amalg$Date))

### Flats
parra_flats_smooth = loess(Flat ~ as.numeric(Date), data = census_amalg, span = 0.5)
census_amalg$Flat = predict(parra_flats_smooth, as.Date(census_amalg$Date))

### Other houses
parra_other_smooth = loess(Other ~ as.numeric(Date), data = census_amalg, span = 0.5)
census_amalg$Other = predict(parra_other_smooth, as.Date(census_amalg$Date))

### Not Stated
parra_ns_smooth = loess(Not.Stated ~ as.numeric(Date), data = census_amalg, span = 0.5)
census_amalg$Not.Stated = predict(parra_ns_smooth, as.Date(census_amalg$Date))

### Surface Area
parra_sa_smooth = loess(Surface.Area ~ as.numeric(Date), data = census_amalg, span = 0.1)
census_amalg$Surface.Area = predict(parra_sa_smooth, as.numeric(census_amalg$Date))

### Pop
parra_pop_smooth = loess(Population ~ as.numeric(Date), data = census_amalg, span = 0.5)
census_amalg$Population = predict(parra_pop_smooth, as.numeric(census_amalg$Date))

### Pop
parra_car_smooth = loess(cars ~ as.numeric(Date), data = census_amalg, span = 0.5)
census_amalg$cars = predict(parra_car_smooth, as.numeric(census_amalg$Date))

write.csv(census_amalg, "katoomba/bom_slga_census_sm.csv", row.names = F)
summary(census_amalg)

ggplot(data = census_amalg, aes(x = Date, y = Surface.Area)) +
  geom_point() +
  geom_smooth(method = 'gam') +
  labs(title = "Census Housing data (Seperated houses)")

min(na.omit(census_amalg$Surface.Area))

census_amalg[is.na(census_amalg$Surface.Area), ]
```

# Export Dataset to CSV
------------------------------------------------------------------------------------------------------
Amalgamate the 3 Cities together
```{r}
pen = read.csv("penrith/bom_slga_census_sm.csv")
par = read.csv("parra/bom_slga_census_sm.csv")
kat = read.csv("katoomba/bom_slga_census_sm.csv")

pen$city = "penrith"
par$city = "parramatta"
kat$city = "katoomba"

min(na.omit(kat$Surface.Area))

amalg_final = rbind(pen, par, kat)
amalg_final$Date = as.Date(amalg_final$Date)

min(na.omit(amalg_final$Surface.Area))

write.csv(amalg_final, "uhi_dataset.csv", row.names = F)
```

